import os
import sqlite3
from collections import OrderedDict

from flask import g

from irepair import APP_ROOT, db

DATABASE = os.path.join(APP_ROOT, 'db.db')


def query_all(query, args=()):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return rv


def query_one(query, args=()):
    cur = get_db().execute(query, args)
    rv = cur.fetchone()
    cur.close()
    return rv


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
        db.row_factory = sqlite3.Row

    return db


def defect_has_colors(repair):
    return repair in ('broken-screen', 'power-button', 'other')


data_dict = OrderedDict([
    ('7-plus', {'name': '7 Plus',
                'repairs': OrderedDict(
                    [('broken-screen', {'price': {'rut': '999', 'no-rut': '1499'}, 'time': '30'}),
                     ('battery', {'price': {'rut': '499', 'no-rut': '649'}, 'time': '15', 'no_color': True}),
                     ('water-damage', {'price': {'rut': '449', 'no-rut': '699'}, 'time': '60', 'no_color': True}),
                     ('power-button', {'price': {'rut': '349', 'no-rut': '499'}, 'time': '30'}),
                     ('other', {'price': None, 'time': None})]),
                'colors': ['rose-gold', 'silver', 'gold', 'black', 'jet-black', 'red']
                }),
    ('7', {'name': '7',
           'repairs': OrderedDict(
               [('broken-screen', {'price': {'rut': '749', 'no-rut': '1099'}, 'time': '30'}),
                ('battery', {'price': {'rut': '449', 'no-rut': '599'}, 'time': '15', 'no_color': True}),
                ('water-damage', {'price': {'rut': '449', 'no-rut': '699'}, 'time': '60', 'no_color': True}),
                ('power-button', {'price': {'rut': '349', 'no-rut': '499'}, 'time': '30'}),
                ('other', {'price': None, 'time': None})]),
           'colors': ['rose-gold', 'silver', 'gold', 'black', 'jet-black', 'red']
           }),
    ('6s-plus', {'name': '6s Plus',
                 'repairs': OrderedDict(
                     [('broken-screen', {'price': {'rut': '699', 'no-rut': '999'}, 'time': '30'}),
                      ('battery', {'price': {'rut': '399', 'no-rut': '549'}, 'time': '15', 'no_color': True}),
                      ('water-damage', {'price': {'rut': '449', 'no-rut': '699'}, 'time': '60', 'no_color': True}),
                      ('power-button', {'price': {'rut': '349', 'no-rut': '499'}, 'time': '30'}),
                      ('other', {'price': None, 'time': None})]),
                 'colors': ['gold', 'space-gray', 'silver', 'rose-gold']
                 }),
    ('6s', {'name': '6s',
            'repairs': OrderedDict(
                [('broken-screen', {'price': {'rut': '599', 'no-rut': '849'}, 'time': '30'}),
                 ('battery', {'price': {'rut': '399', 'no-rut': '549'}, 'time': '15', 'no_color': True}),
                 ('water-damage', {'price': {'rut': '449', 'no-rut': '699'}, 'time': '60', 'no_color': True}),
                 ('power-button', {'price': {'rut': '349', 'no-rut': '499'}, 'time': '30'}),
                 ('other', {'price': None, 'time': None})]),
            'colors': ['gold', 'space-gray', 'silver', 'rose-gold']
            }),
    ('6-plus', {'name': '6 Plus',
                'repairs': OrderedDict(
                    [('broken-screen', {'price': {'rut': '649', 'no-rut': '899'}, 'time': '30'}),
                     ('battery', {'price': {'rut': '399', 'no-rut': '599'}, 'time': '15', 'no_color': True}),
                     ('water-damage', {'price': {'rut': '449', 'no-rut': '699'}, 'time': '60', 'no_color': True}),
                     ('power-button', {'price': {'rut': '349', 'no-rut': '499'}, 'time': '30'}),
                     ('other', {'price': None, 'time': None})]),
                'colors': ['gold', 'space-gray', 'silver', 'rose-gold']
                }),
    ('6', {'name': '6',
           'repairs': OrderedDict(
               [('broken-screen', {'price': {'rut': '549', 'no-rut': '799'}, 'time': '30'}),
                ('battery', {'price': {'rut': '349', 'no-rut': '549'}, 'time': '15', 'no_color': True}),
                ('water-damage', {'price': {'rut': '449', 'no-rut': '699'}, 'time': '60', 'no_color': True}),
                ('power-button', {'price': {'rut': '349', 'no-rut': '499'}, 'time': '30'}),
                ('other', {'price': None, 'time': None})]),
           'colors': ['gold', 'space-gray', 'silver', 'rose-gold']
           }),
    ('se', {'name': 'SE',
            'repairs': OrderedDict(
                [('broken-screen', {'price': {'rut': '449', 'no-rut': '649'}, 'time': '30'}),
                 ('battery', {'price': {'rut': '349', 'no-rut': '499'}, 'time': '15', 'no_color': True}),
                 ('water-damage', {'price': {'rut': '449', 'no-rut': '649'}, 'time': '60', 'no_color': True}),
                 ('power-button', {'price': {'rut': '349', 'no-rut': '449'}, 'time': '30'}),
                 ('other', {'price': None, 'time': None})]),
            'colors': ['gold', 'space-gray', 'silver', 'rose-gold']
            }),
    ('5s', {'name': '5s',
            'repairs': OrderedDict(
                [('broken-screen', {'price': {'rut': '449', 'no-rut': '649'}, 'time': '30'}),
                 ('battery', {'price': {'rut': '349', 'no-rut': '499'}, 'time': '15', 'no_color': True}),
                 ('water-damage', {'price': {'rut': '449', 'no-rut': '649'}, 'time': '60', 'no_color': True}),
                 ('power-button', {'price': {'rut': '349', 'no-rut': '449'}, 'time': '30'}),
                 ('other', {'price': None, 'time': None})]),
            'colors': ['space-gray', 'gold', 'silver']
            }),
    ('5c', {'name': '5c',
            'repairs': OrderedDict(
                [('broken-screen', {'price': {'rut': '449', 'no-rut': '649'}, 'time': '30'}),
                 ('battery', {'price': {'rut': '349', 'no-rut': '499'}, 'time': '15', 'no_color': True}),
                 ('water-damage', {'price': {'rut': '449', 'no-rut': '649'}, 'time': '60', 'no_color': True}),
                 ('power-button', {'price': {'rut': '349', 'no-rut': '449'}, 'time': '30'}),
                 ('other', {'price': None, 'time': None})]),
            'colors': ['white', 'blue', 'green', 'yellow', 'pink']
            }),
    ('5', {'name': '5',
           'repairs': OrderedDict(
               [('broken-screen', {'price': {'rut': '449', 'no-rut': '649'}, 'time': '30'}),
                ('battery', {'price': {'rut': '349', 'no-rut': '499'}, 'time': '15', 'no_color': True}),
                ('water-damage', {'price': {'rut': '449', 'no-rut': '649'}, 'time': '60', 'no_color': True}),
                ('power-button', {'price': {'rut': '349', 'no-rut': '449'}, 'time': '30'}),
                ('other', {'price': None, 'time': None})]),
           'colors': ['black', 'white']
           }),
    ('4s', {'name': '4s',
            'repairs': OrderedDict(
                [('broken-screen', {'price': {'rut': '449', 'no-rut': '649'}, 'time': '30'}),
                 ('battery', {'price': {'rut': '349', 'no-rut': '449'}, 'time': '15', 'no_color': True}),
                 ('water-damage', {'price': {'rut': '449', 'no-rut': '649'}, 'time': '60', 'no_color': True}),
                 ('power-button', {'price': {'rut': '349', 'no-rut': '449'}, 'time': '30'}),
                 ('other', {'price': None, 'time': None})]),
            'colors': ['black', 'white']
            }),
])
cities_dict = OrderedDict([('stockholm', {'name': 'Stockholm',
                                          'maps_coverage': 'https://www.google.com/maps/d/viewer?mid=1UCmopV32aZjJWNcyM00zngyIsgg&ll=59.35620858588053%2C18.0052185089844&z=11'}),
                           ('norrkoping', {'name': 'Norrköping',
                                           'maps_coverage': 'https://www.google.com/maps/d/viewer?mid=1N0OhVAlbKVzGBt4vav3VExHrtzE'}),
                           ('linkoping', {'name': 'Linköping',
                                          'maps_coverage': 'https://drive.google.com/open?id=1-xBSRmf_MTTV5dSWXi8oUL5xF10'})])


def simple_unslugify(slug):
    return slug.replace('_', ' ').replace('-', ' ').title()
