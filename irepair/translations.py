from flask import request, url_for
from functools import reduce
from markupsafe import Markup

from irepair import app
from irepair.data import get_db, query_one
from irepair.slug_translations import translatable_slugs, trans_slug


def create_translation(text_id, text, lang):
    get_db().execute('INSERT OR REPLACE INTO text ("text_id", "text", "lang") VALUES (?,?,?)', (text_id, text, lang))
    get_db().commit()


def trans(text_id, lang=None, edit_arg=None, is_div=None, gen_entry=False):
    lang = lang or 'en'
    text_row = query_one("select text from text where text_id=? and lang=?", (text_id, lang))
    if not text_row and gen_entry:
        create_translation(text_id, text_id, lang)

    text = text_row['text'] if text_row else text_id
    edit_wrapper = '<{0} class="edit7" data-text-id="%s" data-lang="%s">%s</{0}>'.format('div' if is_div else 'span')

    return Markup(text if not edit_arg else edit_wrapper % (text_id, lang, text))


def base_url(lang=None):
    base = ''
    if lang:
        base = base + '/' + lang
    return base


def is_editable():
    return request.args.get('edit') == 'sng'


@app.context_processor
def inject_editability():
    return {'is_editable': is_editable()}


app.jinja_env.filters['trans'] = trans
app.jinja_env.filters['base_url'] = base_url


def ir_url_for(endpoint, lang, **kwargs):
    kw_translated = {k: trans_slug(lang, v) for k, v in kwargs.items() if k in translatable_slugs}
    kwargs.update(kw_translated, lang=lang)
    return url_for(endpoint, **kwargs)


def repair_url(endpoint, lang, **kwargs):
    return ir_url_for(endpoint, lang, action='repair', **kwargs)


def dict_replace(string, r_dict):
    i = 0
    while '{' in string and i < 6:
        string = reduce(lambda x, k: x.replace(k, r_dict[k]), r_dict, string)
        i += 1

    if i == 6:
        app.logger.error('dict_replace probable infinite loop')

    return string

app.jinja_env.filters['ir_url'] = ir_url_for
app.jinja_env.filters['repair_url'] = repair_url
app.jinja_env.filters['dict_replace'] = dict_replace

