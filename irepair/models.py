import enum
import os

from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import validates
from sqlalchemy.orm.exc import NoResultFound
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.utils import secure_filename

from irepair import db, app
from irepair.func import slugify


class Iconized(object):
    @hybrid_property
    def icon_file(self):
        return self.icon

    @icon_file.setter
    def icon_file(self, file):
        if file:
            self.icon = Image(file, '%s' % self.__tablename__)


class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String(80), unique=True)
    email = db.Column(db.String(120), unique=True)
    pw_hash = db.Column(db.String(80))
    is_active = db.Column(db.Boolean())

    @property
    def is_authenticated(self):
        return True

    def __init__(self, username, email, password, **_):
        self.email = email
        self.username = username
        self.set_password(password)
        self.is_active = True
        self.is_anonymous = False

    def set_password(self, password):
        self.pw_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.pw_hash, password)

    def get_id(self):
        return self.username


class ModelGroup(db.Model):
    __tablename__ = 'model_group'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(40), unique=True)
    slug = db.Column(db.String(40), unique=True)

    def __init__(self, name=None):
        self.name = name

    @validates('name')
    def update_slug(self, _, name):
        if name and not self.slug:
            self.slug = slugify(name)
        return name

    @classmethod
    def by_name(cls, name):
        return cls.query.filter(cls.slug == slugify(name)).one_or_none() or cls(name)


class Model(db.Model, Iconized):
    __tablename__ = 'model'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(40), unique=True)
    slug = db.Column(db.String(40), unique=True)
    model_group_id = db.Column(db.Integer, db.ForeignKey('model_group.id'), nullable=True)
    model_group = db.relationship('ModelGroup', backref=db.backref('models', lazy='dynamic'))
    icon_id = db.Column(db.Integer, db.ForeignKey('image.id'), nullable=True)
    icon = db.relationship('Image')
    order_number = db.Column(db.Integer, unique=True, nullable=True)

    colors = association_proxy('color_models', 'color')
    defects = association_proxy('defect_models', 'defect')
    model_group_name = association_proxy('model_group', 'name', creator=ModelGroup.by_name)

    def __init__(self, name=None, filename=None):
        self.name = name
        self.filename = filename

    def __str__(self):
        return self.model_group.name + ' ' + self.name

    @classmethod
    def by_slug(cls, slug):
        return cls.query.filter(cls.slug == slug).one()

    # @hybrid_property
    # def colors(self):
    #     return [color.color for color in self.color_models]
    #
    # @colors.setter
    # def colors(self, value):
    #     self.color_models = [get_color_model(color, self.id) for color in value]

    @validates('name')
    def update_slug(self, _, name):
        if name and not self.slug:
            from irepair.func import slugify
            self.slug = slugify(name)
        return name

    __mapper_args__ = {
        "order_by": order_number
    }


class Defect(db.Model, Iconized):
    __tablename__ = 'defect'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(40), unique=True)
    repair = db.Column(db.String(40), unique=True)
    slug = db.Column(db.String(40), unique=True)
    order_number = db.Column(db.Integer, unique=True, nullable=False)
    icon_id = db.Column(db.Integer, db.ForeignKey('image.id'))
    icon = db.relationship('Image')
    needs_color = db.Column(db.Boolean)

    def __init__(self, name=None):
        self.name = name

    def __str__(self):
        return self.name

    @classmethod
    def by_slug(cls, slug):
        return cls.query.filter(cls.slug == slug).one()

    @validates('name')
    def update_slug(self, _, name):
        if name and not self.slug:
            from irepair.func import slugify
            self.slug = slugify(name)
        return name

    __mapper_args__ = {
        "order_by": order_number
    }


class Image(db.Model):
    __tablename__ = 'image'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    filename = db.Column(db.String(120))
    folder = db.Column(db.String(120))

    def __init__(self, file, folder):
        from werkzeug.datastructures import FileStorage
        filestorage = file if isinstance(file, FileStorage) else FileStorage(file)
        self.folder = folder
        self.filename = secure_filename(filestorage.filename)
        self.save(filestorage)

    @property
    def url(self):
        from flask import url_for
        return url_for('static', filename='uploads/%s' % self.path, _external=True)

    @property
    def path(self):
        return '%s/%s' % (self.folder, self.filename)

    def save(self, file):
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], self.path))


class Color(enum.IntEnum):
    space_gray = 0
    silver = 1
    gold = 2
    rose_gold = 3
    black = 4
    white = 5
    blue = 6
    green = 7
    yellow = 8
    pink = 9
    jet_black = 10
    red = 11


class ColorModel(db.Model):
    __tablename__ = 'color_model'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    model_id = db.Column(db.Integer, db.ForeignKey('model.id'))
    model = db.relationship('Model', backref=db.backref('color_models'))
    color = db.Column(db.Enum(Color), index=True)
    order_number = db.Column(db.Integer)

    def __init__(self, color=None):
        self.color = color

    # ToDo: marked for deletion
    @classmethod
    def by_color_and_model_id(cls, color, model_id=None):
        color_model = None
        if model_id:
            color_model = cls.query.filter(cls.color == color, cls.model_id == model_id).one_or_none()

        return color_model if color_model else cls(color=color)


class DefectModel(db.Model):
    __tablename__ = 'defect_model'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    model_id = db.Column(db.Integer, db.ForeignKey('model.id'))
    model = db.relationship('Model', backref=db.backref('defect_models', lazy='dynamic'))
    model_slug = association_proxy('model', 'slug')

    defect_id = db.Column(db.Integer, db.ForeignKey('defect.id'))
    defect = db.relationship('Defect', backref=db.backref('defect_models', lazy='dynamic'))
    defect_slug = association_proxy('defect', 'slug')

    price = db.Column(db.Integer)
    price_rut = db.Column(db.Integer)

    @classmethod
    def by_slugs(cls, model_slug, defect_slug):
        return cls.query.filter(cls.model_slug == model_slug, cls.defect_slug == defect_slug).one()

    def __str__(self):
        return '%s | %s' % (self.defect, self.model)


class RepairService(db.Model):
    """ aka. Product """
    __tablename__ = 'repair_service'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    defect_model_id = db.Column(db.Integer, db.ForeignKey('defect_model.id'), nullable=False)
    defect_model = db.relationship('DefectModel', backref=db.backref('repair_services', lazy='dynamic'))
    color = db.Column(db.Enum(Color))
    pipedrive_id = db.Column(db.Integer, unique=True)

    price = association_proxy('defect_model', 'price')
    price_rut = association_proxy('defect_model', 'price_rut')

    model_slug = association_proxy('defect_model', 'model_slug')
    defect_slug = association_proxy('defect_model', 'defect_slug')

    @classmethod
    def by_slugs_and_color(cls, model_slug, defect_slug, color):
        return cls.query.filter(cls.model_slug == model_slug, cls.defect_slug == defect_slug, cls.color == color).one()

    @classmethod
    def get_by_defect_model_and_color(cls, defect_model, color):
        try:
            return cls.query.filter(cls.defect_model == defect_model, cls.color == color).one()
        except NoResultFound:
            new_object = cls(defect_model=defect_model, color=color)
            db.session.add(new_object)
            return new_object

    def __str__(self):
        """{Repair} | {Device Line} {Model} | {Color}"""
        from irepair.translations import trans
        return '%s | %s' % (self.defect_model.defect.repair, self.defect_model.model)\
               + (' | %s' % trans(self.color.name, 'en') if self.color else '')


class TranslationType(enum.IntEnum):
    text = 0
    slug = 1


# see translate_and_check and slug_translations.py
class Translation(db.Model):
    __tablename__ = 'translation'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    original = db.Column(db.String(40))
    translated = db.Column(db.String(40))
    translation_type = db.Column(db.Enum(TranslationType), index=True)
    language = db.Column(db.String(2))  # ToDo: use enum?


class Person(db.Model):
    __tablename__ = 'person'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    first_name = db.Column(db.String(80))
    last_name = db.Column(db.String(80))
    phone = db.Column(db.String(80))
    email = db.Column(db.String(80))
    pipedrive_id = db.Column(db.Integer)
    langs = association_proxy('orders', 'lang')

    @classmethod
    def find_or_create(cls, **kw):
        persons = cls.query.filter(*[getattr(cls, field) == value for field, value in kw.items()]).all()
        return persons[0] if persons else Person(**kw)


class Order(db.Model):
    __tablename__ = 'order'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    person_id = db.Column(db.Integer, db.ForeignKey('person.id'))
    person = db.relationship('Person', backref=db.backref('orders', lazy='dynamic'))
    is_rut = db.Column(db.Boolean)
    location = db.Column(db.String(20))
    city = db.Column(db.String(80))
    lang = db.Column(db.String(10))
    meet_location = db.Column(db.String)
    meet_time = db.Column(db.String)
    pipedrive_id = db.Column(db.Integer, unique=True)

    @property
    def total_price(self):
        return sum(cart_item.price for cart_item in self.cart_items)

    @hybrid_property
    def first_cart_item(self):
        return self.cart_items[0] if self.cart_items else None

    @first_cart_item.setter
    def first_cart_item(self, cart_item):
        if len(self.cart_items):
            self.cart_items[0] = cart_item
        else:
            self.cart_items = [cart_item]

    @property
    def template_dict(self):
        template_dict = {k: v for k, v in self.__dict__.items() if not k.startswith('_') and k != 'lang'}
        template_dict.update({
            'person': self.person,
            'defect': self.first_cart_item.defect_model.defect if self.first_cart_item else None,
            'model': self.first_cart_item.defect_model.model if self.first_cart_item else None
        })
        return template_dict


class CartItem(db.Model):
    def __init__(self, **kwargs):
        super(CartItem, self).__init__(**kwargs)
        if not self.quantity:
            self.quantity = 1

    __tablename__ = 'cart_item'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    order_id = db.Column(db.Integer, db.ForeignKey('order.id'))
    order = db.relationship('Order', backref=db.backref('cart_items'))
    defect_model_id = db.Column(db.Integer, db.ForeignKey('defect_model.id'))
    defect_model = db.relationship('DefectModel')
    quantity = db.Column(db.Integer, default=1)
    color = db.Column(db.Enum(Color))
    is_rut = association_proxy('order', 'is_rut')
    other_details = db.Column(db.String)
    pipedrive_id = db.Column(db.Integer, unique=True)

    @property
    def price_per_item(self):
        return self.defect_model.price if not self.is_rut else self.defect_model.price_rut

    @property
    def full_price_no_vat_per_item(self):
        return (self.price_per_item * (2 if self.is_rut else 1) - 50) / 1.25 if self.price_per_item else 0

    @hybrid_property
    def repair_service(self):
        return RepairService.get_by_defect_model_and_color(defect_model=self.defect_model, color=self.color)

    @repair_service.setter
    def repair_service(self, repair_service):
        self.defect_model = repair_service.defect_model
        self.color = repair_service.color

    @property
    def price(self):
        return self.quantity * self.price_per_item if self.price_per_item else 0

    # def __init__(self, defect_model):
    #     self.defect_model = defect_model
