import json
import locale
import os

from flask import Flask
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy
from markupsafe import Markup
from werkzeug.contrib.cache import SimpleCache
from werkzeug.routing import BaseConverter

from irepair.slug_translations import trans_slug
from irepair.tasks import make_celery

app = Flask(__name__)


def get_env_config():
    return {
        'dev': {
            'spreadshit_id': '1ulgpVir92jiwl160UP68hLUraJdVT4jzVDP5g4xs3oo',
            'lead_in_stage_id': 19,
            'mail_prefix': '[DEV]'
        },
        'sta': {
            'spreadshit_id': '1ulgpVir92jiwl160UP68hLUraJdVT4jzVDP5g4xs3oo',
            'lead_in_stage_id': 19,
            'mail_prefix': '[STA]'
        },
        'live': {
            'spreadshit_id': '1T2bakplW171UHhou3CDZMK9nYCEC-fZLZ3-dwXHVkf4',
            'lead_in_stage_id': 1,
            'mail_prefix': ''
        },
    }[os.environ.get('ENV', 'dev')]

ENV_CONFIG = get_env_config()

locale.setlocale(locale.LC_ALL, 'de_DE.UTF-8')
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
DOMAIN = 'http://localhost:5000'
SHARED_SECRET = 'kFVfQBTjhiZTo3R'
EID = '7706'
DIR_7SYNC = os.path.join(APP_ROOT, 'static', '7synced')

app.config.update(dict(
    DEBUG=os.environ.get('ENV', 'dev') != 'live',
    SECRET_KEY='It"saNewEra!:P4thos3whorbr4v3w1ll5urv1v3!',
    MAIL_SERVER='localhost',
    MAIL_PORT=25,
    MAIL_USE_TLS=True,
    MAIL_USE_SSL=False,
    MAIL_DEBUG=app.debug,
    MAIL_USERNAME='fixme',
    MAIL_PASSWORD='sng12348765',
    MAIL_DEFAULT_SENDER=None,
    MAIL_MAX_EMAILS=None,
    MAIL_SUPPRESS_SEND=app.testing,
    MAIL_ASCII_ATTACHMENTS=False,
    CELERY_BROKER_URL='amqp://guest:guest@localhost:5672//',
    CELERY_RESULT_BACKEND='redis://localhost:6379',
    CELERY_TASK_SERIALIZER='pickle',
    CELERY_ACCEPT_CONTENT=['pickle'],
    SQLALCHEMY_TRACK_MODIFICATIONS=False,
    SQLALCHEMY_DATABASE_URI='postgresql://fixuser:%s@localhost/%s' % (
        os.environ.get('FIXSER_DB_PASS', 'a12348765'), os.environ.get('FIXSER_DB', 'fixdb')),
    UPLOAD_FOLDER=os.path.join(os.path.dirname(os.path.realpath(__file__))) + '/static/uploads'
))

mail = Mail(app)
db = SQLAlchemy(app)

ADMINS = ['gpoussif@gmail.com', 'it@fixser.se']
mail_handler = None
if not app.debug:
    import logging
    from logging.handlers import SMTPHandler
    mail_handler = SMTPHandler(app.config.get('MAIL_SERVER', '127.0.0.1'), 'it@fixser.se',
                               ADMINS, 'YourApplication Failed',
                               credentials=(app.config.get('MAIL_USERNAME'), app.config.get('MAIL_PASSWORD')))
    mail_handler.setLevel(logging.ERROR)
    app.logger.addHandler(mail_handler)

celery = make_celery(app)

cache = SimpleCache()


class LangConverter(BaseConverter):
    def __init__(self, url_map):
        super(LangConverter, self).__init__(url_map)
        self.regex = '(?:en|se)'

app.url_map.converters['lang'] = LangConverter

locations = ['home-rut', 'home-no-rut', 'cafe', 'workplace']


class LocationConverter(BaseConverter):
    def __init__(self, url_map):
        super().__init__(url_map)
        self.regex = '(?:%s)' % '|'.join(locations + [trans_slug('se', loc) for loc in locations])

app.url_map.converters['location'] = LocationConverter

page_names = ['warranty', 'about-us', 'coverage']


class PageNameConverter(BaseConverter):
    def __init__(self, url_map):
        super().__init__(url_map)
        self.regex = '(?:%s)' % '|'.join(page_names + [trans_slug('se', name) for name in page_names])

app.url_map.converters['page'] = PageNameConverter

actions = ['repair']


class ActionConverter(BaseConverter):
    def __init__(self, url_map):
        super().__init__(url_map)
        self.regex = '(?:%s)' % '|'.join(actions + [trans_slug('se', action) for action in actions])

app.url_map.converters['action'] = ActionConverter

cities = ['stockholm', 'norrkoping']


class CityConverter(BaseConverter):
    def __init__(self, url_map):
        super().__init__(url_map)

        from irepair.data import cities_dict
        self.regex = '(?:%s)' % '|'.join(cities_dict.keys())

app.url_map.converters['city'] = CityConverter


class HomeConverter(BaseConverter):
    def __init__(self, url_map):
        super().__init__(url_map)
        self.regex = '(?:%s)' % '|'.join(['home', trans_slug('se', 'home')])

app.url_map.converters['home'] = HomeConverter

DEFAULT_CITY = 'all_cities'
site_data = {'all_cities': {'phone_number': '0734-126372',
                            'analytics': 'UA-91027014-1',
                            'email': 'fixme@fixser.se',
                            'name': 'Fixser',
                            'logo': 'Fixser-Logo-white.png',
                            'logo_float': 'Fixser-Logo-blue.png',
                            'favicon': 'Fixser-favicon-2.png',
                            'domain': 'fixser.se',
                            'copyright': Markup('2017 Fixser &#8480;'),
                            'maps_coverage': None,
                            'crawlable_city': 'stockholm'},
             'norrkoping': {'city': 'Norrköping',
                            'phone_number': '0734-126372',
                            'analytics': 'UA-91027014-1',
                            'email': 'fixme@fixser.se',
                            'name': 'Fixser Norrköping',
                            'domain': 'norrkoping.fixser.se',
                            'copyright': Markup('2017 Fixser Norrköping &#8480;'),
                            'maps_coverage': 'https://www.google.com/maps/d/viewer?mid=1N0OhVAlbKVzGBt4vav3VExHrtzE'},
             'stockholm': {'city': 'Stockholm',
                           'phone_number': '0734-126372',
                           'analytics': 'UA-91027014-1',
                           'email': 'fixme@fixser.se',
                           'name': 'Fixser Stockholm',
                           'domain': 'stockholm.fixser.se',
                           'copyright': Markup('2017 Fixser Stockholm &#8480;'),
                           'maps_coverage': 'https://www.google.com/maps/d/viewer?mid=1UCmopV32aZjJWNcyM00zngyIsgg&ll=59.35620858588053%2C18.0052185089844&z=11'}}

users = {'sng123': 'sng123', 'viktor': 'billigteknik'}

@app.context_processor
def inject_site():
    from flask import request
    return {'site': site_data[get_city_from_url(request.url_root)]}


@app.context_processor
def inject_cities():
    from irepair.data import cities_dict
    return {'cities_dict': cities_dict}


def load_7sync_file(file):
    with open(os.path.join(DIR_7SYNC, 'files.json'), 'r') as files_data_file:
        files_data = json.load(files_data_file)

    from flask import url_for
    return url_for('static', filename='7synced/%s' % file) + '?v=%s' % files_data.get(file, '1')


def get_city_from_url(url):
    return 'all_cities'

app.jinja_env.filters['load_7sync'] = load_7sync_file

import irepair.translations
import irepair.admin
import irepair.views
import irepair.translations_views
