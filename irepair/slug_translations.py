import os
from itertools import groupby

from sqlalchemy.exc import ProgrammingError, InternalError

slug_translations = {
    'se': {
        'repair': 'laga',
        'broken-screen': 'trasig-skarm',
        'battery': 'batteri',
        'water-damage': 'vattenskada',
        'power-button': 'powerknapp',
        'other': 'ovriga',
        'home': 'hemma',
        'home-rut': 'hemma-med-rut',
        'home-no-rut': 'hemma-utan-rut',
        'cafe': 'kafe',
        'workplace': 'arbetsplats',
        'warranty': 'garanti',
        'about-us': 'om-oss',
        'coverage': 'tackningskarta',
        # 'gold': 'gold_se',
        # 'space-gray': 'space-gray_se',
        # 'silver': 'silver_se',
        # 'rose-gold': 'rose-gold_se',
        # 'black': 'black_se',
        # 'white': 'white_se',
        # 'blue': 'blue_se',
        # 'green': 'green_se',
        # 'yellow': 'yellow_se',
        # 'pink': 'pink_se',
    }
}
slug_detranslations = {lang: {v: k for k, v in slug_translations[lang].items()} for lang in slug_translations}

translatable_slugs = ('action', 'repair', 'color', 'home', 'location', 'page')


def get_slug_translations():
    from irepair import cache
    slug_translations = cache.get('slug_translations')
    if not slug_translations:
        slug_translations = update_slug_translations_cache()

    return slug_translations


def get_slug_detranslations():
    return {lang: {v: k for k, v in slug_translations[lang].items()} for lang in get_slug_translations()}


def update_slug_translations_cache():
    from irepair import cache
    from irepair.models import Translation, TranslationType
    slug_translations = {}
    try:
        slug_translations_from_db = Translation.query.filter(
            Translation.translation_type == TranslationType.slug).order_by('language').all()

        for k, group in groupby(slug_translations_from_db, lambda t: t.language):
            slug_translations[k] = {t.original: t.translated for t in group}
        cache.set('slug_translations', slug_translations)
    except ProgrammingError as e:
        print(e)
        print('relation "translation" does not exist' in str(e))
    except InternalError as e:
        print(e)
        print('current transaction is aborted, commands ignored' in str(e))
    except Exception as e:
        print(e)
        print('current transaction is aborted, commands ignored' in str(e))

    return slug_translations


def trans_slug(lang, slug):
    slug_translations = get_slug_translations()
    return slug_translations[lang].get(slug, False) if slug_translations.get(lang) else slug


def detrans_slug(lang, t_slug):
    slug_detranslations = get_slug_detranslations()
    return slug_detranslations[lang].get(t_slug, False) if slug_detranslations.get(lang) else t_slug


def detrans_slugs(lang, *args):
    return tuple(detrans_slug(lang, slug) for slug in args)
