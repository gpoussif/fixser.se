import re
from functools import wraps
from unicodedata import normalize

from flask import Response
from flask import abort
from flask import request
from sqlalchemy.orm.exc import NoResultFound

from irepair import page_names, actions, locations, users
from irepair.data import data_dict
from irepair.slug_translations import translatable_slugs, detrans_slug
from irepair.translations import ir_url_for


def check_slugs(action=None, location=None, home=None, page=None, **_):
    if action is not None and action not in actions:
        return False
    if page is not None and page not in page_names:
        return False
    if home is not None and home != 'home':
        return False
    if location is not None and location not in locations:
        return False
    return True


def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response('Could not verify your access level for that URL.\nYou have to login with proper credentials',
                    401, {'WWW-Authenticate': 'Basic realm="Login Required"'})


def translate_transform_and_check(**transformers):
    def real_translate_transform_and_check(f):
        @wraps(f)
        def decorated(lang, *args, **kwargs):
            # Turn slugs into their original (English) version
            kw_translated = {k: detrans_slug(lang, v) for k, v in kwargs.items() if k in translatable_slugs}
            kwargs.update(kw_translated)

            kwargs['switch_lng_url'] = ir_url_for(f.__name__, 'en' if lang == 'se' else 'se', _external=True, **kwargs)
            kwargs['lang'] = lang
            if not check_slugs(**kwargs):
                abort(404)
            for kwarg, transformer in transformers.items():
                try:
                    kwargs[kwarg] = transformer(**kwargs)
                except NoResultFound:
                    print(kwarg)
                    import inspect
                    print({kw: kwargs[kw] for kw in inspect.signature(transformer).parameters if kw in kwargs})
                    abort(404)

            return f(*args, **kwargs)

        return decorated
    return real_translate_transform_and_check


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not is_auth():
            return authenticate()
        return f(*args, **kwargs)

    return decorated


def is_auth():
    auth = request.authorization
    return auth and auth.username in users.keys() and auth.password == users[auth.username]


_punct_re = re.compile(r'[\t !"#$%&\'()*\-/<=>?@\[\\\]^_`{|},.]+')


def slugify(text, delim=u'-'):
    """Generates an slightly worse ASCII-only slug."""
    result = []
    for word in _punct_re.split(text.lower()):
        if word:
            result.append(word)
    return str(delim.join(result))
