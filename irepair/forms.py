from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import StringField, IntegerField, SelectMultipleField, BooleanField
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.validators import DataRequired, InputRequired
from wtforms.widgets import HiddenInput

from irepair.models import Model, Defect, Color


class ModelForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    icon_file = FileField('Icon', validators=[])
    model_group_name = StringField('Model Group', validators=[DataRequired()])
    order_number = IntegerField('Order Number', widget=HiddenInput(), validators=[InputRequired()])
    colors = SelectMultipleField('Colors', choices=[(color.value, name) for name, color in Color.__members__.items()],
                                 coerce=lambda v: Color(int(v)))


class DefectForm(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    order_number = IntegerField('Order Number', widget=HiddenInput(), validators=[InputRequired()])
    repair = StringField('Repair', validators=[DataRequired()])
    needs_color = BooleanField('Needs Color', )
    icon_file = FileField('Icon')


class DefectModelForm(FlaskForm):
    model = QuerySelectField('Model', query_factory=Model.query.all)
    defect = QuerySelectField('Defect', query_factory=Defect.query.all)
    price = IntegerField('Price')
    price_rut = IntegerField('PriceRUT')

#
#
# class DefectModelForm(FlaskForm):
#     model = QuerySelectField('Model', query_factory=Model.query)
#     repair = QuerySelectField('Defect', query_factory=Defect.query)
#     price = IntegerField('Price')
#     price_rut = IntegerField('PriceRUT')

