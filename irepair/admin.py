from enum import Enum

from flask import request, render_template, redirect

from irepair import app, db
from irepair.api_calls.pipedrive import send_product_to_pipedrive
from irepair.forms import ModelForm, DefectForm, DefectModelForm
from irepair.func import requires_auth
from irepair.models import Model, Defect, Image, DefectModel, RepairService

mapping_slug2model_form = {
    'models': {'model': Model, 'form': ModelForm},
    'defects': {'model': Defect, 'form': DefectForm},
    'defect_models': {'model': DefectModel, 'form': DefectModelForm},
}


@app.route('/admin', methods=['GET'])
@requires_auth
def admin_dashboard():
    return render_template('admin/dashboard.html', model_names=mapping_slug2model_form)


@app.route('/admin/list/<model>', methods=['GET'])
@requires_auth
def admin_models(model):
    model_form_dict = mapping_slug2model_form[model]
    model_class = model_form_dict['model']
    query = model_class.query.order_by(model_class.order_number) if hasattr(model_class,
                                                                            'order_number') else model_class.query

    return render_template('admin/models.html', form=model_form_dict['form'](), model=model, field_parser=field_parser,
                           objects=query.all() or [])


@app.route('/admin/list/<model>', methods=['POST'])
@requires_auth
def admin_models_post(model):
    model_form_dict = mapping_slug2model_form[model]
    form = model_form_dict['form'](csrf_enabled=False)
    if request.form and form.validate():
        model = model_form_dict['model']()
        form.populate_obj(model)
        db.session.add(model)
        db.session.commit()
    else:
        print(form.errors)

    print(request.form)
    return redirect(request.url)


@app.route('/admin/edit/<model>/<id>', methods=['GET'])
@requires_auth
def admin_model_edit(model, id):
    model_form_dict = mapping_slug2model_form[model]
    object = model_form_dict['model'].query.get(id)

    return render_template('admin/%s_edit.html' % model if not request.is_xhr else 'admin/components/row_form.html',
                           form=model_form_dict['form'](obj=object), object=object,
                           model_class=model_form_dict['model'])


@app.route('/admin/edit/<model>/<id>', methods=['POST'])
@requires_auth
def admin_model_edit_post(model, id):

    model_form_dict = mapping_slug2model_form[model]
    form = model_form_dict['form'](csrf_enabled=False)
    model_instance = None
    # ToDo: refactor
    if request.form and form.validate():
        model_instance = model_form_dict['model'].query.get(id)

        form.populate_obj(model_instance)
        db.session.add(model_instance)
        db.session.commit()
    else:
        print(form.errors)

    print(request.form)
    if request.is_xhr and model_instance:
        return render_template('admin/components/model_row.html', model=model, object=model_instance, form=form,
                               field_parser=field_parser)
    return redirect(request.url)


@app.route('/admin/generate_products', methods=['GET'])
@requires_auth
def admin_generate_products():
    defect_models = DefectModel.query.all()
    a = ''
    for defect_model in defect_models:
        colors2add = defect_model.model.colors if defect_model.defect.needs_color else [None]
        for color in colors2add:
            repair_service = RepairService.get_by_defect_model_and_color(defect_model=defect_model, color=color)
            a += str(repair_service) + '<br>'
    db.session.commit()
    return a


@app.route('/admin/sync_pipedrive/products', methods=['GET'])
@requires_auth
def admin_sync_pipedrive_products():
    unsynced_repair_services = RepairService.query.filter(RepairService.pipedrive_id.is_(None)).all()

    missing_repair_text = []
    for repair_service in unsynced_repair_services:
        if not repair_service.defect_model.defect.repair:
            missing_repair_text.append(repair_service)
            continue

        send_product_to_pipedrive(repair_service)

    db.session.commit()
    return '%d out of %d products not synced to pipedrive because of defects without repair text' % (
            len(missing_repair_text), len(unsynced_repair_services))


def field_parser(value):
    if isinstance(value, Image):
        return '<img src="%s" height="60">' % value.url

    if isinstance(value, Enum):
        return str(value.name)

    from jinja2.tests import test_sequence
    if test_sequence(value) and not isinstance(value, str):
        return ', '.join((field_parser(item) for item in value))

    return str(value)
