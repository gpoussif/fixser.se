from flask import render_template, request, jsonify

from irepair import app
from irepair.data import query_all
from irepair.func import requires_auth, authenticate, is_auth
from irepair.translations import create_translation, trans, is_editable


@app.route('/edit7', methods=['POST'])
def edit7():
    create_translation(request.form.get('text_id'), request.form.get('text'), request.form.get('lang', 'en'))

    return jsonify({'txt': trans(request.form.get('text')), 'code': 200})


@app.route('/translations')
@requires_auth
def translations_list():
    languages = ['en', 'se']
    translation_keys = query_all("SELECT DISTINCT text_id from text")
    return render_template('translations.html', translation_keys=(t_key['text_id'] for t_key in translation_keys),
                           languages=languages)


@app.before_request
def print_hola(*args, **kwargs):
        auth = request.authorization
        if is_editable() and not is_auth():
            return authenticate()
