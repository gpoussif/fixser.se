$(document).ready(function () {
    $('.color-choice .btn').on('click', function () {
        var $self = $(this), field_name = $self.data('field'), choice_val = $self.data('choice');
        $('[name="' + field_name + '"][value=' + choice_val + ']').prop('checked', true).trigger('change');
    });
});