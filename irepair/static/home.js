/**
 * Created by gab on 13/07/16.
 */
var fh_visible = $('.floating-header header').css('top') == 0;
$(window).on('scroll', function () {
    var aTop = $('section#intro').height() / 3;
    if ($(this).scrollTop() >= aTop) {
        if (!fh_visible) {
            $('.floating-header header').animate({'top': '0'});
            fh_visible = true;
        }
    } else if (fh_visible) {
        $('.floating-header header').animate({'top': '-100%'});
        fh_visible = false;
    }
});
