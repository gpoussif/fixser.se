$(document).on("keypress", function (e) {
    console.log(e.ctrlKey);
    console.log(e.which);
    if (e.ctrlKey && ( e.which === 25 )) {
        $(".edit7").toggleClass('no-border');
    }
    if (e.ctrlKey) {
        if (window.car_pause) {
            $('#carousel-example-generic').carousel('cycle');
        } else {
            $('#carousel-example-generic').carousel('pause');
            window.car_pause = true;
        }
    }
});
isEditabilitySet = false;

$(document).ready(function () {
    isEditabilitySet = true;
    $('.edit7').on('click', makeEditable);
});

setTimeout(function () {
    if(!isEditabilitySet) {
        isEditabilitySet = true;
        $('.edit7').on('click', makeEditable);
    }
}, 1000);
var makeEditable = function (e) {
    e.preventDefault();
    e.stopPropagation();
    var $editTag = $(this), height = $editTag.height(), width = $editTag.width();
    var oldText = $editTag.html();
    $editTag.closest('a,button').bind('click', function (e) {
        e.preventDefault();
    });
    console.log(width, height);
    $editTag.off('click', makeEditable);
    $editTag.html('<textarea style="color:black"></textarea>');
    var $textarea = $editTag.find('textarea');
    $textarea.height(height).width(width);
    if (oldText) $textarea.val(oldText);
    $textarea.on('keypress', function (e) {
        if (e.which == 13) {
            $.post('/edit7', {
                'text_id': $editTag.data('text-id'),
                'lang': $editTag.data('lang'),
                'text': $(this).val(),
                'is-md': $editTag.data('is-md')
            });
            $editTag.html($(this).val());
            $editTag.on('click', makeEditable);
        }
    });
};