$(document).ready(function (e) {
    $(document).on('dblclick', '[data-dbl-click-load]', function() {
        $.get($(this).data('dbl-click-load')).then((resp) => this.outerHTML = resp);
    });
    $(document).on('submit', 'form[data-ajax-submit]', function(e) {
        e.preventDefault();
        let form = this;
        $.ajax({
            type: form.method || 'POST',
            url: form.action || window.location.href, // whether the form action or the current page
            data: new FormData(form), // our data object
            processData: false, // need to set to false so files don't get meddled with
            contentType: false // need to set to false so jquery wont meddle with this
        }).then((response) => form.outerHTML = response);
        // }).then((response) => console.log(response));
        return false;
    })
});
