from celery import Celery
from redis import RedisError


def make_celery(app):
    celery = Celery(app.import_name, backend=app.config['CELERY_RESULT_BACKEND'],
                    broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask

    return celery


def try_celery(task):
    def try_wrapper(*args, **kwargs):
        import os
        # Todo: understand celery, rabbitmq and Redis and make staging work
        if os.environ.get('ENV') == 'sta' or kwargs.get('_no_celery'):
            if kwargs.get('_no_celery'):
                del kwargs['_no_celery']
            task(*args, **kwargs)
            return 'Not a task'

        try:
            return task.delay(*args, **kwargs)
        except RedisError:
            print('RedisError with %s' % task.__name__)
            task(*args, **kwargs)
            return 'Not a task'

    return try_wrapper
