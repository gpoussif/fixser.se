from celery.utils.log import get_task_logger

from irepair import celery, db, ENV_CONFIG, app
from irepair.api_calls.pipedrive_client import Pipedrive
from irepair.models import Color, Order, RepairService
from irepair.tasks import try_celery

PIPEDRIVE_API_KEY = "916c3e07a739c6ae2e678ce8833405b057b807f6"
pipedrive = Pipedrive(PIPEDRIVE_API_KEY)
task_logger = get_task_logger(__name__)


@try_celery
@celery.task(serializer='pickle')
def send_order_to_pipedrive(order_id):
    order = Order.query.get(order_id)
    if not order:
        task_logger.error('[PIPEDRIVE] order %d not found' % (order_id))
        return

    if order.pipedrive_id:
        task_logger.error('[PIPEDRIVE] order %d already has pipedrive_id:%d' % (order.id, order.pipedrive_id))
        return

    try:
        send_person_to_pipedrive(order.person)
    except Exception as e:
        task_logger.error('[PIPEDRIVE] order %d: issues sending person info: %s' % (order_id, e))
        return

    add_deal_request = pipedrive.deals(
        {'person_id': order.person.pipedrive_id,
         'stage_id': ENV_CONFIG['lead_in_stage_id'],
         '8ba77210204234a557f16fd37de76a775c5f6ca6': city_short_dict.get(order.city, order.city),
         'title': "%s's iPhone %s [%s %s - %s]" % (
             order.person.first_name, order.first_cart_item.defect_model.model.name,
             get_part_color(order.first_cart_item),
             order.first_cart_item.defect_model.defect.name,
             city_short_dict.get(order.city, order.city)),
         'value': order.total_price, },
        method='POST')

    print('[PIPEDRIVE] request sent. Answer:%s' % str(add_deal_request))
    if not add_deal_request.get('data') or not add_deal_request['data'].get('id'):
        task_logger.error('[PIPEDRIVE] order %d: MISSING parts in answer: %s' % (order_id, add_deal_request))
        return

    task_logger.info('[PIPEDRIVE] request to add order %d received: %s' % (order_id, str(add_deal_request['data'])))
    order.pipedrive_id = add_deal_request['data']['id']

    add_product_to_pipedrive_deal(order.first_cart_item)

    for activity in activities2add:
        req = pipedrive.activities(dict(deal_id=order.pipedrive_id, **activity), method='POST')

    db.session.commit()


def send_person_to_pipedrive(person):
    # from irepair.models import Person
    # person = Person.query.get(person_id)
    if person.pipedrive_id:
        return person.pipedrive_id

    find_person_request = pipedrive.persons_find({'term': person.email, 'search_by_email': True}, method='GET')

    results = find_person_request['data'] if find_person_request else None

    if not results:
        add_person_request = pipedrive.persons(
            {'name': person.first_name + ' ' + person.last_name,
             'afae42a593a5bc2277a7b414067b64ab039ec464': person.first_name,
             'f6300bb0748f9f7ef95922b9b08aadb3d07b6fe8': person.last_name,
             'email': person.email,
             'phone': person.phone,
             'f4cf7473691abfc35320d2032f0703d3d7af23ef': person.langs[-1]},
            method='POST')

        results = [add_person_request['data']]

    person.pipedrive_id = results[0]['id']


@try_celery
@celery.task(serializer='pickle')
def send_meet_info_to_pipedrive(order_id):
    order = Order.query.get(order_id)
    if not order.pipedrive_id:
        return

    getattr(pipedrive, 'deals_%d' % order.pipedrive_id)({
        'id': order.pipedrive_id,
        'c0fa4a4a374ea979fb97d39b3b88d6eca949ea5f': order.meet_location,
        '58fd76147428fb4427a10a965cfab3c19464c69a': order.meet_time},
        method='PUT')


def add_product_to_pipedrive_deal(cart_item):
    if not cart_item.order.pipedrive_id:
        return

    repair_service = cart_item.repair_service

    if not repair_service.pipedrive_id:
        if not repair_service.defect.repair:
            task_logger.warning("Missing defect.repair. Can't create Pipedrive Product for %d" % repair_service.id)
            return
        send_product_to_pipedrive(repair_service)

    add_product_to_deal_request = getattr(pipedrive, 'deals_%d_products' % cart_item.order.pipedrive_id)({
        'product_id': repair_service.pipedrive_id,
        'item_price': cart_item.full_price_no_vat_per_item,
        'quantity': cart_item.quantity},
        method='POST')

    cart_item.pipedrive_id = add_product_to_deal_request['data']['id']


city_short_dict = {'stockholm': 'STHLM', 'norrkoping': 'NKPG', 'linkoping': 'LKPG'}


def send_product_to_pipedrive(product):
    """ :type product: irepair.models.RepairService"""
    from irepair.translations import trans

    request_data = {'name': str(product),
                    '08865cc11fce91329732d3f03ac9c0cdc62ba102': product.price_rut,
                    '1ef6a139dcef275bcea674246f310c3082ceb86d': product.defect_model.defect.repair,
                    '78615355bffda6285979cbce77304abc781da974': product.defect_model.model.model_group.name,
                    '28c6c725974c1bbd899c603fcdb46ab98846d029': product.defect_model.model.name,
                    'tax': 25,
                    }
    if product.price_rut:
        request_data['prices'] = [{'currency': 'SEK', 'price': (product.price_rut * 2 - 50) / 1.25}]
    if product.color:
        request_data['706634789d2073f1a5125bf75a3fea227ae2d2eb'] = trans(product.color.name, 'en')

    add_product_request = pipedrive.products(request_data, method='POST')

    product.pipedrive_id = add_product_request['data']['id']


color2part_color = {Color.space_gray: 'Black',
                    Color.silver: 'White',
                    Color.gold: 'White',
                    Color.rose_gold: 'White',
                    Color.black: 'Black',
                    Color.white: 'White',  # (for iPhone 5; Black for iPhone 5c)
                    Color.blue: 'Black',
                    Color.green: 'Black',
                    Color.yellow: 'Black',
                    Color.pink: 'Black',
                    Color.jet_black: 'Black',
                    Color.red: 'White'}


def get_part_color(cart_item):
    if cart_item.color == Color.white and cart_item.defect_model.model.slug == '5c':
        return 'Black'

    return color2part_color[cart_item.color] if cart_item.color else ''


activities2add = [
    {'subject': 'First contact', 'type': 'task'},
    {'subject': '(a) Knock-on-door', 'type': 'task'},
    {'subject': '(b) Repair started', 'type': 'task'},
    {'subject': '(c) Repair finished', 'type': 'task'},
    {'subject': '(d) Out-of-house', 'type': 'task'},
    {'subject': 'Faktura sent', 'type': 'task'}
]
