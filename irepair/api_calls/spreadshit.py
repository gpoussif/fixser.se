import os

import httplib2
from googleapiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

from irepair import celery, ENV_CONFIG

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/sheets.googleapis.com-python-quickstart.json
from irepair.tasks import try_celery

SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Google Sheets API Python Quickstart'


def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.dirname(os.path.realpath(__file__))
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'sheets.googleapis.com-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME

        credentials = tools.run_flow(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials


@try_celery
@celery.task(serializer='pickle')
def append_row_to_spreadshit(row_array, range_name):
    """Shows basic usage of the Sheets API.

    Creates a Sheets API service object and prints the names and majors of
    students in a sample spreadsheet:
    https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
    """
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    discovery_url = 'https://sheets.googleapis.com/$discovery/rest?version=v4'
    service = discovery.build('sheets', 'v4', http=http, discoveryServiceUrl=discovery_url)

    result = service.spreadsheets().values().append(
        spreadsheetId=ENV_CONFIG['spreadshit_id'], range=range_name, valueInputOption='USER_ENTERED',
        body={'values': [row_array]}
    ).execute()

    if not result:
        print('No data found.')
    else:
        print('Result est: ', result)


def send_initial_data_to_spreadshit(order):
    from irepair.translations import trans
    from irepair.data import cities_dict
    row_data = [
        order.person.first_name,
        order.person.last_name,
        order.person.email,
        order.person.phone,
        order.first_cart_item.defect_model.model_slug,
        'iPhone ' + order.first_cart_item.defect_model.model.name,
        order.first_cart_item.defect_model.defect_slug,
        trans(order.first_cart_item.defect_model.defect_slug, order.lang),
        order.first_cart_item.color.name if order.first_cart_item.color else '',
        trans(order.first_cart_item.color.name, order.lang) if order.first_cart_item.color else '',
        cities_dict[order.city]['name'] if cities_dict.get(order.city) else '',
        order.location,
        trans(order.location, order.lang),
        order.is_rut,
        order.lang,
        order.first_cart_item.price,
        order.first_cart_item.other_details,
        order.id
    ]

    append_row_to_spreadshit(row_array=row_data, range_name='Orders!A:R')


# RowId	Meet Time	Meet Location
def send_extra_data_to_spreadshit(order):
    append_row_to_spreadshit(row_array=[order.id, order.meet_time, order.meet_location], range_name='ExtraData!A1:C1')


def send_city_suggestion_to_spreadshit(form, lang):
    append_row_to_spreadshit(row_array=[form.get('fullname'), form.get("email"), form.get("city"), lang],
                             range_name='CitySuggest!A1:D1')
