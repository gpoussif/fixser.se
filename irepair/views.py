import logging
from flask import request, session, redirect, url_for, render_template
from redis import RedisError

from irepair import app, db, mail_handler
from irepair import mail_deliver
from irepair.api_calls import spreadshit, pipedrive
from irepair.data import get_db, defect_has_colors
from irepair.func import translate_transform_and_check, requires_auth
from irepair.models import Model, DefectModel, Order, Color, CartItem, Person
from irepair.translations import repair_url, trans, ir_url_for


@app.route('/robots.txt')
def robots_txt():
    ret = """User-agent: * \nAllow: /"""

    from flask import Response
    resp = Response(response=ret,
                    status=200,
                    mimetype="text/plain")

    return resp


@app.route('/sitemap.xml')
def sitemap_xml():
    ret = render_template('sitemap.xml')

    from flask import Response
    resp = Response(response=ret,
                    status=200,
                    mimetype="text/xml")

    return resp


@app.route('/hm')
def hola_mundo():
    return "Hola Mundo!"


#
# @app.route('/')
# def redirect_to_main_language():
#     return redirect(url_for('home', lang='se'))


@app.route('/', defaults={'lang': 'se'})
@app.route('/<lang:lang>/')
@app.route('/<city:city>', defaults={'lang': 'se'})
@app.route('/<lang:lang>/<city:city>')
def home(lang=None, city=None):
    return render_template('home.html', lang=lang, city=city)


@app.route('/tacknings/')
def tacknings_redirect():
    return redirect(url_for('page_view', page='tackningskarta', lang='se'), code=301)


@app.route('/<page:page>/', defaults={'lang': 'se'})
@app.route('/<lang:lang>/<page:page>/')
@translate_transform_and_check()
def page_view(lang=None, page=None, switch_lng_url=None):
    return render_template('layout_flow/%s.html' % page, lang=lang, switch_lng_url=switch_lng_url)


@app.route('/<action:action>/iphone/', defaults={'lang': 'se'})
@app.route('/<lang:lang>/<action:action>/iphone/')
@translate_transform_and_check()
def choose_city(action, lang=None, switch_lng_url=None):
    return render_template('layout_flow/select_city.html', lang=lang, switch_lng_url=switch_lng_url)


@app.route('/<city:city>/<action:action>/iphone', defaults={'lang': 'se'})
@app.route('/<lang:lang>/<city:city>/<action:action>/iphone')
@translate_transform_and_check()
def choose_model(action, city, lang=None, switch_lng_url=None):
    backlink = repair_url('choose_city', lang)
    return render_template('layout_flow/select_model.html', city=city, models=Model.query.all(), lang=lang,
                           switch_lng_url=switch_lng_url)


@app.route('/<city:city>/<action:action>/iphone/<model>', defaults={'lang': 'se'})
@app.route('/<lang:lang>/<city:city>/<action:action>/iphone/<model>')
@translate_transform_and_check(model=lambda **kw: Model.by_slug(slug=kw['model']))
def choose_defect(action, city, model, lang=None, switch_lng_url=None):
    backlink = repair_url('choose_model', lang, city=city)
    return render_template('layout_flow/select_defect.html', city=city, model=model, backlink=backlink, lang=lang,
                           switch_lng_url=switch_lng_url)


@app.route('/<city:city>/<action:action>/iphone/<model>/<defect>', defaults={'lang': 'se'})
@app.route('/<lang:lang>/<city:city>/<action:action>/iphone/<model>/<defect>')
@translate_transform_and_check(defect_model=lambda model, defect, **_: DefectModel.by_slugs(model, defect))
def choose_location(action, city, model, defect, lang=None, switch_lng_url=None, defect_model=None):
    backlink = repair_url('choose_defect', lang, city=city, model=defect_model.model_slug)
    return render_template('layout_flow/select_location.html', city=city, defect=defect_model.defect,
                           model=defect_model.model, backlink=backlink, lang=lang, switch_lng_url=switch_lng_url)


@app.route('/<city:city>/<action:action>/iphone/<model>/<defect>/<home:home>', defaults={'lang': 'se'})
@app.route('/<lang:lang>/<city:city>/<action:action>/iphone/<model>/<defect>/<home:home>')
@translate_transform_and_check(defect_model=lambda model, defect, **_: DefectModel.by_slugs(model, defect))
def choose_rut(action, city, model, defect, home, lang=None, switch_lng_url=None, defect_model=None):
    backlink = repair_url('choose_defect', lang, city=city, model=defect_model.model_slug)
    return render_template('layout_flow/select_rut.html', city=city, defect=defect_model.defect,
                           model=defect_model.model, backlink=backlink, lang=lang, switch_lng_url=switch_lng_url)


@app.route('/<city:city>/<action:action>/iphone/<model>/<defect>/<location:location>', defaults={'lang': 'se'},
           methods=['POST', 'GET'])
@app.route('/<lang:lang>/<city:city>/<action:action>/iphone/<model>/<defect>/<location:location>',
           methods=['POST', 'GET'])
@translate_transform_and_check(defect_model=lambda model, defect, **_: DefectModel.by_slugs(model, defect))
def quote(action, city, model, defect, location, lang=None, switch_lng_url=None, defect_model=None):
    order = Order(city=city, location=location, is_rut=location == 'home-rut', lang=lang)
    order.first_cart_item = CartItem(defect_model=defect_model)
    if request.method == 'POST':
        order.first_cart_item.color = Color[request.form.get('color')] if request.form.get('color') else None

        order.person = Person.find_or_create(email=request.form.get('email'), first_name=request.form.get('first_name'),
                                             last_name=request.form.get('last_name'), phone=request.form.get('phone'))
        db.session.add(order.person)
        db.session.add(order)
        db.session.commit()
        session.update({'order_id': order.id})

        mail_deliver.pre_internal_mail(order_id=order.id)
        spreadshit.send_initial_data_to_spreadshit(order=order)
        pipedrive.send_order_to_pipedrive(order_id=order.id)

        return redirect(url_for('confirmation', lang=lang))
    backlink = repair_url('choose_location', lang, city=order.city, model=defect_model.model_slug,
                          defect=defect_model.defect_slug)
    return render_template('layout_flow/quote.html', lang=lang, is_quote=True, backlink=backlink,
                           cart_item=order.first_cart_item, switch_lng_url=switch_lng_url, **order.template_dict)


@app.route('/confirm', defaults={'lang': 'se'}, methods=['POST', 'GET'])
@app.route('/<lang:lang>/confirm', methods=['GET', 'POST'])
def confirmation(lang=None):
    if not session.get('order_id'):
        return redirect(url_for('home'))
    order = Order.query.get(session['order_id'])
    if request.method == 'POST' or order.first_cart_item.defect_model.defect.slug == 'other':
        order.meet_location = request.form.get('meet_location')
        order.meet_time = request.form.get('meet_time')
        db.session.commit()

        mail_deliver.internal_mail(order_id=order.id)
        mail_deliver.external_mail(order_id=order.id)
        pipedrive.send_meet_info_to_pipedrive(order_id=order.id)

        if request.method == 'POST':
            spreadshit.send_extra_data_to_spreadshit(order=order)

        return redirect(url_for('thanks', lang=lang))
    switch_lng_url = ir_url_for('confirmation', 'en' if lang == 'se' else 'se')
    return render_template('layout_flow/confirmation.html', lang=lang, no_footer=True, cart_item=order.first_cart_item,
                           switch_lng_url=switch_lng_url, **order.template_dict)


@app.route('/thanks', defaults={'lang': 'se'})
@app.route('/<lang:lang>/thanks', methods=['GET', 'POST'])
def thanks(lang=None):
    if not session.get('order_id'):
        return redirect(url_for('home'))

    order = Order.query.get(session['order_id'])

    return render_template('layout_flow/thanks.html', order=order, lang=lang, no_footer=True,
                           switch_lng_url=ir_url_for('thanks', 'en' if lang == 'se' else 'se'))


@app.route('/submit_city', defaults={'lang': 'se'}, methods=['POST'])
@app.route('/<lang:lang>/submit_city', methods=['POST'])
def submit_city(lang=None):
    a = mail_deliver.city_suggest(form_data=request.form, lang=lang)
    print(a.task_id)

    get_db().execute('INSERT OR REPLACE INTO city_suggest ("fullname", "email", "city", "lang") VALUES (?,?,?,?)',
                     (request.form.get("first_name"), request.form.get("email"), request.form.get("city"), lang))

    get_db().commit()
    b = spreadshit.send_city_suggestion_to_spreadshit(form=request.form, lang=lang)
    print(b.task_id if b else b)

    return trans("Thank you for your input. We'll keep you posted", lang, gen_entry=True)


@app.route('/<lang:lang>/tsv')
@requires_auth
def tsv_dump(lang):
    return sv_dump('\t', lang)


@app.route('/send2pipedrive')
@requires_auth
def send_to_pipedrive():
    non_synched_orders = Order.query.filter(Order.pipedrive_id.is_(None)).all()
    db.session.expunge_all()
    if non_synched_orders:
        for order in non_synched_orders:
            pipedrive.send_order_to_pipedrive(order_id=order.id, _no_celery=True)

    return 'Sent %d orders' % len(non_synched_orders)


@app.route('/<lang:lang>/csv')
@requires_auth
def csv_dump(lang):
    return sv_dump(',', lang)


def sv_dump(separator, lang):
    result = 'model{SEP}defect{SEP}color{SEP}price\n'.replace('{SEP}', separator)
    row_pattern = 'iPhone %s{SEP}%s{SEP}%s{SEP}%s\n'.replace('{SEP}', separator)

    for defect_model in DefectModel.query.all():
        # if defect:
        #     for color in ('black', 'white'):
        #         result += row_pattern % (data_dict[model]['name'],
        #                                  trans(defect, lang),
        #                                  trans(color, lang),
        #                                  str(data_dict[model]['defects'][defect]['price'] or '???'))
        # else:
        #     result += row_pattern % (data_dict[model]['name'],
        #                              trans(defect, lang),
        #                              'DOES NOT APPLY',
        #                              str(data_dict[model]['defects'][defect]['price'] or '???'))
        pass
    return result

if mail_handler:
    class RequestFormatter(logging.Formatter):
        def format(self, record):
            record.url = request.url
            record.form = str(request.form.to_dict())
            record.remote_addr = request.remote_addr
            return super().format(record)

    formatter = RequestFormatter(
        '[%(asctime)s] %(remote_addr)s requested %(url)s\n'
        'FORM Data: %(form)s\n'
        '%(levelname)s in %(module)s: %(message)s'
    )
    mail_handler.setFormatter(formatter)
