from flask import render_template
from flask import request
from flask_mail import Message
from html2text import html2text
from redis.exceptions import RedisError

from irepair import site_data, mail, celery, get_city_from_url, ENV_CONFIG
from irepair.models import Order
from irepair.translations import trans
mail_prefix = ENV_CONFIG.get('mail_prefix', '')


def send_it(fnc):
    def send_fnc(*args, **kwargs):
        msg = fnc(*args, **kwargs)
        msg.body = html2text(msg.html)
        try:
            return send_mail_task.delay(msg)
        except RedisError as e:
            class A:
                task_id = str(e) + '\n' + '=-=-=-=-\n' * 5 + msg.html
            return A

    return send_fnc


@send_it
def pre_internal_mail(order_id, **_):
    order = Order.query.get(order_id)
    msg = Message("%sEnhorabuena, gilipollas (y Gabriel)! %s wants you to fix their iPhone [%s]" % (
                    mail_prefix, order.person.first_name, order.lang),
                  sender=(site_data[get_city_from_url(request.url_root)]['name'], site_data[get_city_from_url(request.url_root)]['email']),
                  recipients=["santiagobravo1983@gmail.com", "gpoussif@gmail.com"])
    msg.html = render_template('mails/internal_confirmation.html', order=order, lang='en')
    return msg


@send_it
def external_mail(order_id, **_):
    order = Order.query.get(order_id)
    msg = Message("%s%s iPhone %s" % (
                    mail_prefix, trans("Repair Order Received for ", order.lang, gen_entry=True),
                    order.first_cart_item.defect_model.model.name),
                  sender=(site_data[get_city_from_url(request.url_root)]['name'],
                          site_data[get_city_from_url(request.url_root)]['email']),
                  recipients=[order.person.email])
    msg.html = render_template('mails/external_confirmation.html', order=order, lang=order.lang, **order.template_dict)
    return msg


@send_it
def internal_mail(order_id, **_):
    order = Order.query.get(order_id)
    msg = Message("%sLocation and time - %s sent us extra information [%s]" % (
                    mail_prefix, order.person.first_name, order.lang),
                  sender=(site_data[get_city_from_url(request.url_root)]['name'],
                          site_data[get_city_from_url(request.url_root)]['email']),
                  recipients=["santiagobravo1983@gmail.com", "gpoussif@gmail.com"])

    msg.html = render_template('mails/internal_extra_info.html', order=order, lang='en', **order.template_dict)
    return msg


@send_it
def city_suggest(form_data, lang):
    msg = Message("%s%s recommended the city: %s" % (mail_prefix, form_data.get('fullname'), form_data.get('city')),
                  sender=(site_data[get_city_from_url(request.url_root)]['name'], site_data[get_city_from_url(request.url_root)]['email']),
                  recipients=["santiagobravo1983@gmail.com", "gpoussif@gmail.com"])

    msg.html = render_template('mails/city_suggest.html', form_data=form_data, user_lang=lang)
    return msg


@celery.task(serializer='pickle')
def send_mail_task(msg):
    mail.send(msg)
