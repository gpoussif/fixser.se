"""initial

Revision ID: 39475ded6ae8
Revises: 
Create Date: 2017-09-08 13:53:43.309786

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '39475ded6ae8'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('image',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('filename', sa.String(length=120), nullable=True),
                    sa.Column('folder', sa.String(length=120), nullable=True),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_table('model_group',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('name', sa.String(length=40), nullable=True),
                    sa.Column('slug', sa.String(length=40), nullable=True),
                    sa.PrimaryKeyConstraint('id'),
                    sa.UniqueConstraint('name'),
                    sa.UniqueConstraint('slug')
                    )
    op.create_table('order',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('firstname', sa.String(length=80), nullable=True),
                    sa.Column('lastname', sa.String(length=80), nullable=True),
                    sa.Column('phone', sa.String(length=80), nullable=True),
                    sa.Column('email', sa.String(length=80), nullable=True),
                    sa.Column('is_rut', sa.Boolean(), nullable=True),
                    sa.Column('location', sa.String(length=20), nullable=True),
                    sa.Column('city', sa.String(length=80), nullable=True),
                    sa.Column('lang', sa.String(length=10), nullable=True),
                    sa.Column('meet_location', sa.String(), nullable=True),
                    sa.Column('meet_time', sa.String(), nullable=True),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_table('translation',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('original', sa.String(length=40), nullable=True),
                    sa.Column('translated', sa.String(length=40), nullable=True),
                    sa.Column('translation_type', sa.Enum('text', 'slug', name='translationtype'), nullable=True),
                    sa.Column('language', sa.String(length=2), nullable=True),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_index(op.f('ix_translation_translation_type'), 'translation', ['translation_type'], unique=False)
    op.create_table('user',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('username', sa.String(length=80), nullable=True),
                    sa.Column('email', sa.String(length=120), nullable=True),
                    sa.Column('pw_hash', sa.String(length=80), nullable=True),
                    sa.Column('is_active', sa.Boolean(), nullable=True),
                    sa.PrimaryKeyConstraint('id'),
                    sa.UniqueConstraint('email'),
                    sa.UniqueConstraint('username')
                    )
    op.create_table('model',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('name', sa.String(length=40), nullable=True),
                    sa.Column('slug', sa.String(length=40), nullable=True),
                    sa.Column('model_group_id', sa.Integer(), nullable=True),
                    sa.Column('icon_id', sa.Integer(), nullable=True),
                    sa.Column('order_number', sa.Integer(), nullable=True),
                    sa.ForeignKeyConstraint(['icon_id'], ['image.id'], ),
                    sa.ForeignKeyConstraint(['model_group_id'], ['model_group.id'], ),
                    sa.PrimaryKeyConstraint('id'),
                    sa.UniqueConstraint('name'),
                    sa.UniqueConstraint('order_number'),
                    sa.UniqueConstraint('slug')
                    )
    op.create_table('repair',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('name', sa.String(length=40), nullable=True),
                    sa.Column('slug', sa.String(length=40), nullable=True),
                    sa.Column('order_number', sa.Integer(), nullable=False),
                    sa.Column('icon_id', sa.Integer(), nullable=True),
                    sa.Column('needs_color', sa.Boolean(), nullable=True),
                    sa.ForeignKeyConstraint(['icon_id'], ['image.id'], ),
                    sa.PrimaryKeyConstraint('id'),
                    sa.UniqueConstraint('name'),
                    sa.UniqueConstraint('order_number'),
                    sa.UniqueConstraint('slug')
                    )
    from irepair.models import Color
    op.create_table('color_model',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('model_id', sa.Integer(), nullable=True),
                    sa.Column('color', sa.Enum(Color), nullable=True),
                    sa.Column('order_number', sa.Integer(), nullable=True),
                    sa.ForeignKeyConstraint(['model_id'], ['model.id'], ),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_index(op.f('ix_color_model_color'), 'color_model', ['color'], unique=False)
    op.create_table('repair_model',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('model_id', sa.Integer(), nullable=True),
                    sa.Column('repair_id', sa.Integer(), nullable=True),
                    sa.Column('price', sa.Integer(), nullable=True),
                    sa.Column('price_rut', sa.Integer(), nullable=True),
                    sa.ForeignKeyConstraint(['model_id'], ['model.id'], ),
                    sa.ForeignKeyConstraint(['repair_id'], ['repair.id'], ),
                    sa.PrimaryKeyConstraint('id')
                    )
    op.create_table('cart_item',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('order_id', sa.Integer(), nullable=True),
                    sa.Column('repair_model_id', sa.Integer(), nullable=True),
                    sa.Column('quantity', sa.Integer(), nullable=True),
                    sa.Column('color', sa.Enum(Color), nullable=True),
                    sa.Column('other_details', sa.String(), nullable=True),
                    sa.ForeignKeyConstraint(['order_id'], ['order.id'], ),
                    sa.ForeignKeyConstraint(['repair_model_id'], ['repair_model.id'], ),
                    sa.PrimaryKeyConstraint('id')
                    )


def downgrade():
    op.drop_table('cart_item')
    op.drop_table('repair_model')
    op.drop_index(op.f('ix_color_model_color'), table_name='color_model')
    op.drop_table('color_model')
    op.drop_table('repair')
    op.drop_table('model')
    op.drop_table('user')
    op.drop_index(op.f('ix_translation_translation_type'), table_name='translation')
    op.drop_table('translation')
    op.drop_table('order')
    op.drop_table('model_group')
    op.drop_table('image')
