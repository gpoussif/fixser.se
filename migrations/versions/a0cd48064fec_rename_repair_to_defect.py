"""RENAME: repair to defect

Revision ID: a0cd48064fec
Revises: 85e99a7f071c
Create Date: 2017-09-23 12:27:19.167457

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a0cd48064fec'
down_revision = '85e99a7f071c'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('repair_model', 'repair_id', new_column_name='defect_id')
    op.rename_table('repair', 'defect')
    op.rename_table('repair_model', 'defect_model')
    from irepair.data import get_db
    get_db().execute("UPDATE text SET text_id='choose_defect' WHERE text_id LIKE 'choose_repair'")
    get_db().commit()


def downgrade():
    op.rename_table('defect_model', 'repair_model')
    op.rename_table('defect', 'repair')
    op.alter_column('repair_model', 'defect_id', new_column_name='repair_id')
    from irepair.data import get_db
    get_db().execute("UPDATE text SET text_id='choose_repair' WHERE text_id LIKE 'choose_defect'")
    get_db().commit()

