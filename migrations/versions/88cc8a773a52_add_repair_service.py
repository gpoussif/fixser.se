"""ADD: repair_service (and fix cart_item reference to defect_model)

Revision ID: 88cc8a773a52
Revises: a0cd48064fec
Create Date: 2017-09-23 15:26:54.610947

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects.postgresql import ENUM

# revision identifiers, used by Alembic.
revision = '88cc8a773a52'
down_revision = 'a0cd48064fec'
branch_labels = None
depends_on = None


def upgrade():
    from irepair.models import Color
    op.create_table('repair_service',
                    sa.Column('id', sa.Integer(), nullable=False),
                    sa.Column('defect_model_id', sa.Integer(), nullable=False),
                    sa.Column('color', ENUM(Color, create_type=False), nullable=True),
                    sa.Column('pipedrive_id', sa.Integer(), nullable=True),
                    sa.ForeignKeyConstraint(['defect_model_id'], ['defect_model.id'], ),
                    sa.PrimaryKeyConstraint('id'),
                    sa.UniqueConstraint('pipedrive_id')
                    )
    op.add_column('defect', sa.Column('repair', sa.String(length=40), nullable=True))
    op.create_unique_constraint('defect_repair_unique', 'defect', ['repair'])

    op.drop_constraint('cart_item_repair_model_id_fkey', 'cart_item', type_='foreignkey')
    op.alter_column('cart_item', 'repair_model_id', new_column_name='defect_model_id')
    op.create_foreign_key('cart_item_defect_model_id_fkey', 'cart_item', 'defect_model', ['defect_model_id'], ['id'])


def downgrade():
    op.drop_constraint('cart_item_defect_model_id_fkey', 'cart_item', type_='foreignkey')
    op.alter_column('cart_item', 'repair_model_id', new_column_name='defect_model_id')
    op.create_foreign_key('cart_item_repair_model_id_fkey', 'cart_item', 'defect_model', ['repair_model_id'], ['id'])

    op.drop_constraint('defect_repair_unique', 'defect', type_='unique')
    op.drop_column('defect', 'repair')
    op.drop_table('repair_service')
