"""ADD: pipedrive_id to cart_item

Revision ID: b70796106bd1
Revises: 88cc8a773a52
Create Date: 2017-09-24 17:04:57.868300

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b70796106bd1'
down_revision = '88cc8a773a52'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('cart_item', sa.Column('pipedrive_id', sa.Integer(), nullable=True))
    op.create_unique_constraint(None, 'cart_item', ['pipedrive_id'])


def downgrade():
    op.drop_constraint(None, 'cart_item', type_='unique')
    op.drop_column('cart_item', 'pipedrive_id')
