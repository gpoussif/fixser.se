import re
from urllib import request

from scrapping.db import conn
from scrapping.rq import make_allabolag_request

urls = []

for i in range(1, 30):
    req = make_allabolag_request(
        '/verksamhet/reparation-av-kommunikationsutrustning/95120?page=' + str(i)
    )
    with request.urlopen(req) as rq:
        # print(str(rq.read()))
        companies_htmls = str(rq.read()).replace('\n', '').replace('\\n', '').split(
            '<h2 class="search-results__item__title">')[1:]
        for company_html in companies_htmls:
            urls.append((re.search('<a href="(http://www.allabolag.se/[0-9][^"]+)">', company_html.strip()).group(1),))

print(urls)
conn.executemany('INSERT INTO company (url_allabolag) VALUES (?)', urls)
conn.commit()
conn.close()
