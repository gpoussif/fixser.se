import csv
import html

from scrapping.db import query_all


table = 'company'
for table in ['company', 'company_year']:
    fields = query_all('PRAGMA table_info(%s)' % table)
    companies2fill = query_all('SELECT * FROM %s' % table)

    with open('%s.csv' % table, 'w') as f:
        writer = csv.writer(f)
        print([c[1] for c in fields])
        writer.writerow([c[1] for c in fields])
        writer.writerows(([html.unescape(c) if type(c) is str else c for c in r] for r in companies2fill))

