import os
import sqlite3

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
DATABASE = os.path.join(APP_ROOT, 'bolags.db')
conn = sqlite3.connect(DATABASE)


def query_all(query, args=()):
    cur = conn.cursor()
    cur.execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return rv


def insert(table, fields=(), values=()):
    # g.db is the database connection
    cur = conn.cursor()
    query = 'INSERT INTO %s (%s) VALUES (%s)' % (
        table,
        ', '.join(fields),
        ', '.join(['?'] * len(values))
    )
    cur.execute(query, values)
    conn.commit()
    id = cur.lastrowid
    cur.close()
    return id


def update(table, cond, values):
    # g.db is the database connection
    cur = conn.cursor()
    query = 'UPDATE %s SET %s WHERE %s' % (
        table,
        ', '.join(field + '=?' for field in values.keys()),
        cond
    )
    cur.execute(query, list(values.values()))
    conn.commit()
    cur.close()
