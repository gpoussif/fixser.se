import re
from urllib import request

import html
from scrapping.rq import make_allabolag_request
from scrapping.db import update, query_all

dt_pattern_types = [
    r'<dt>\s*FIELD_NAME\s*</dt>\s*<dd>([^<]+)</dd>',
    r'<dt>\s*FIELD_NAME\s*</dt>\s*<dd>\s*<a [^>]+>([^<]+)</a>\s*</dd>',
    r'<dt>\s*FIELD_NAME\s*</dt>\s*<dd>\s*([^<]+)<a [^>]+>[^<]+</a>\s*</dd>',
]


def extract_company(html):
    # generic_patterns = {
    #     'antal_anstallda': r'<dt>\s*ANTAL ANSTÄLLDA\s*</dt>\s*<dd>([^<]+)</dd>',
    #     'omsattning': r'<dt>\s*OMSÄTTNING\s*</dt>\s*<dd>([^<]+)</dd>',
    #     'year_last_report': r'momsredovisning som företaget har gjort\s+kalenderår (20[01][0-9])'
    # }
    generic_patterns = {
        'namn': r'<h1 class="p-name">([^<]+)</h1>',
        'org_nummer': r'<span>Org.nummer: ([^<]+)</span>',
        'telefon': r'<a class="p-tel" href="tel:([^"]+)">',
        'lan': r'<span class="p-region">([^<]+) l&auml;n</span>',
        'antal_anstallda': r'<th scope="row">Antal anställda</th>\s*<td class="number--[^"]+">\s*([^<]+)</td>',
        'year_last_report': r'Nyckeltal (201[0-9])-[0-9][0-9]?\b',
        'month_last_report': r'Nyckeltal 201[0-9]-([0-9][0-9]?)\b'
    }

    dt_patterns = {
        'bolagsform': {'pattern_type': 0, 'field_name': 'Bolagsform'},
        'moderbolag': {'pattern_type': 1, 'field_name': 'Moderbolag'},
        'registreringsar': {'pattern_type': 0, 'field_name': 'Registreringsår'},
        'f_skatt': {'pattern_type': 2, 'field_name': 'F-Skatt'},
        'moms': {'pattern_type': 0, 'field_name': 'Moms'}
    }
    dt_patterns_normal = {k: dt_pattern_types[v['pattern_type']].replace('FIELD_NAME', v['field_name'])
                          for k, v in dt_patterns.items()}
    generic_patterns.update(dt_patterns_normal)

    h3_base_pattern = '<h3 class="margin-top-none">FIELD_NAME</h3>\s*<p>([^%]+%)'
    h3_patterns = {
        'vinstmarginal': 'Vinstmarginal',
        'kassalikviditet': 'Kassalikviditet',
        'soliditet': 'Soliditet',
        'bruttovinstmarginal': 'Bruttovinstmarginal'
    }
    h3_patterns_normal = {k: h3_base_pattern.replace('FIELD_NAME', v) for k, v in h3_patterns.items()}
    generic_patterns.update(h3_patterns_normal)

    th_base_pattern = '<th scope="row">\s*FIELD_NAME\s*</th>\s*<td class="number--\w+">([^<]+)</td>'
    th_patterns = {
        'omsattning': 'Omsättning',
        'res_e_fin': 'Res. e. fin',
        'arets_resultat': 'Årets resultat',
        'summa_tillgangar': 'Summa tillgångar'
    }
    th_patterns_normal = {k: th_base_pattern.replace('FIELD_NAME', v) for k, v in th_patterns.items()}
    generic_patterns.update(th_patterns_normal)

    company = extract_data(html, generic_patterns)

    vd_match_object = re.search(
        '<dt>\s*VD(?:\s*, Styrelseordf&ouml;rande)?\s*</dt>\s*<dd>\s*<a href="([^"]+)">([^<]+)</a>\s*</dd>', html)
    if vd_match_object:
        company['vd_url'] = vd_match_object.group(1)
        company['vd'] = vd_match_object.group(2).strip()

    address_match_object = re.search(
        '<a class="desktop-only" href="(http://maps.google.se/maps\?q=[^"]+)"\s+target="_blank">([^<]+)<span', html)
    if address_match_object:
        company['url_gmaps'] = address_match_object.group(1)
        company['besoksaddress'] = address_match_object.group(2).strip()

    return company


def extract_verksamhet(html):
    chevron = '<span class="icon icon--chevron-down"></span>'
    bransch_re = chevron + '\s*</h3>\s*<ul class="accordion.+?">\s*<li>\s*<a href="/.+?">\s*([A-Z][^<]+)\s*</a>'
    generic_patterns = {
        'bransch': 'Bransch\s*' + bransch_re,
        'sub_bransch': 'Bransch\s*' + re.sub('[()]', '', bransch_re) + '\s*<ul>.+?([A-Z][^<]+)</a>\s*</li>\s*</ul>',
        'verksamhet_andamal': 'Verksamhet & ändamål\s*' + chevron +
                              '\s*</h3>\s*<p class="accordion-body display-none">\s*([^<]+)</p>',
    }
    dt_patterns = {
        'status': 'Status',
        'bolaget_registrerat': 'Bolaget registrerat',
        'startdatum_for_f_skatt': 'Startdatum för F-Skatt',
        'startdatum_for_moms': 'Startdatum för moms',
        'agandeforhallande': 'Ägandeförhållande'
    }
    dt_patterns_normal = {k: dt_pattern_types[0].replace('FIELD_NAME', v) for k, v in dt_patterns.items()}
    generic_patterns.update(dt_patterns_normal)

    company_verksamhet = extract_data(html, generic_patterns)

    sni_re = re.search('class="[^"]+ sni">.+?</dl>', html)
    if sni_re:
        sni_html = sni_re.group(0)
        snis = re.findall('<dt>([0-9]{5})</dt>', sni_html)
        company_verksamhet['svensk_naringsgrensindelning'] = ', '.join(snis)
        company_verksamhet['sni_amount'] = len(snis)

    return company_verksamhet


def extract_data(html_string, patterns):
    data_dict = {}
    html_string = re.sub('\s+', ' ', html_string.replace('\n', '').replace('\\n', ''))
    for field, pattern in patterns.items():
        match_object = re.search(pattern, html_string)
        if match_object:
            data_dict[field] = html.unescape(re.search(pattern, html_string).group(1).strip())
        else:
            print(pattern)

    return data_dict

# url = 'http://www.allabolag.se/5564081379/office-it-partner-helsingborg-aktiebolag'

def fetch_company_data(url):
    req = make_allabolag_request(url)
    with request.urlopen(req) as rq:
        company = extract_company(rq.read().decode('utf8'))

    # url_verksamhet = re.match('http://www.allabolag.se/[0-9][^/]+/', url).group(0) + 'verksamhet'
    #
    # req = make_allabolag_request(url_verksamhet)
    # with request.urlopen(req) as rq:
    #     company_verksamhet = extract_verksamhet(rq.read().decode('utf8'))
    #     company.update(company_verksamhet)

    if company:
        update('company', 'url_allabolag = "%s"' % url, company)


# companies2fill_urls = query_all('SELECT url_allabolag FROM company WHERE antal_anstallda IS NULL')
companies2fill_urls = query_all('SELECT url_allabolag FROM company WHERE namn IS NULL')
for company2fill in companies2fill_urls:
    fetch_company_data(company2fill[0])
