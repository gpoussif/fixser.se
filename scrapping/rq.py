from urllib import request


def make_allabolag_request(url):
    req = request.Request(url if url[0] != '/' else 'http://www.allabolag.se' + url)
    req.add_header('Referer', 'http://www.allabolag.se/verksamhet/reparation-av-kommunikationsutrustning/95120')
    req.add_header('User-Agent', 'Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11')
    req.add_header('X-Requested-With', 'XMLHttpRequest')
    req.add_header('Host', 'www.allabolag.se')
    return req
