import re
from urllib import request

from scrapping.db import query_all, insert
from scrapping.rq import make_allabolag_request

fields_dict = {
    'Antal anställda': 'antal_anstallda',
    'Omsättning': 'omsattning',
    'Årets resultat': 'arets_resultat',
    'Löner till styrelse & VD': 'loner_till_styrelse_vd',
    'Löner till övriga anställda': 'loner_till_ovriga_anstallda',
    'Vinstmarginal': 'vinstmarginal',
    'Kassalikviditet': 'kassalikviditet',
    'Soliditet': 'soliditet',
    'Resultaträkning': 'resultatrakning',
    'Nettoomsättning': 'nettoomsattning',
    'Övrig omsättning': 'ovrig_omsattning',
    'Rörelseresultat (EBIT)': 'rorelresultat_ebit',
    'Resultat efter finansnetto': 'resultat_efter_finansnetto',
    'Tecknat ej inbetalt kapital': 'tecknat_ej_inbetalt_kapital',
    'Anläggningstillgångar': 'anlaggningstillgangar',
    'Omsättningstillgångar': 'omsattningstillgangar',
    'Tillgångar': 'tillgangar',
    'Eget kapital': 'eget_kapital',
    'Obeskattade reserver': 'obeskattade_reserver',
    'Avsättningar (tkr)': 'avsattningar',
    'Långfristiga skulder': 'langfristiga_skulder',
    'Kortfristiga skulder': 'kortfristiga_skulder',
    'Skulder och eget kapital': 'skulder_och_eget_kapital',
    'Varav tantiem till styrelse & VD': 'varav_tantiem_till_styrelse_vd',
    'Varav resultatlön till övriga anställda': 'varav_resultatlon_till_ovriga_anstallda',
    'Sociala kostnader': 'sociala_kostnader',
    'Utdelning till aktieägare': 'utdelning_till_aktieagare',
    'Nettoomsättning per anställd (tkr)': 'nettoomsattning_per_anstalld',
    'Personalkostnader per anställd (tkr)': 'personalkostnader_per_anstalld',
    'Rörelseresultat, EBITDA': 'rorelseresultat_ebitda',
    'Nettoomsättningförändring': 'nettoomsattningforandring',
    'Du Pont-modellen': 'du_pont_modellen',
    'Bruttovinstmarginal': 'bruttovinstmarginal',
    'Rörelsekapital/omsättning': 'rorelsekapital_omsattning',
}


def extract_years(html=''):
    html = html.replace('\n', '').replace('\\n', '')

    years_dict = {}
    company_years = {}
    for i in range(20):
        year_month_re = '<th class="data-pager__page data-pager__page--%d">\s*(\d{4})-(\d{1,2})' % i
        year_month_match = re.search(year_month_re, html)
        if year_month_match:
            years_dict[i] = year_month_match.group(1)
            company_years[year_month_match.group(1)] = {'year': year_month_match.group(1),
                                                        'month': year_month_match.group(2)}
        else:
            break

    for row in html.split('<tr>')[1:]:
        field_match = re.search('<th scope="row">(.+?)</th>', row)
        if field_match:
            field_name = re.sub('</?span[^>]*>', '', field_match.group(1)).strip()
            current_field = fields_dict.get(field_name)
            if current_field:
                for page, year in years_dict.items():
                    value_re = '<td class="number--[a-z]+ data-pager\w+ data-pager\w+--%d">\s*([^<]+)\s*</td>' % page
                    value_match = re.search(value_re, row)
                    if value_match:
                        company_years[year][current_field] = value_match.group(1)
    return company_years


def fetch_company_data(url, org_nummer, namn):
    req = make_allabolag_request(url)
    with request.urlopen(req) as rq:
        std_html = rq.read().decode('utf8')

    url_bokslut = re.match('http://www.allabolag.se/[0-9][^/]+/', url).group(0) + 'bokslut'
    req = make_allabolag_request(url_bokslut)
    with request.urlopen(req) as rq:
        bokslut_html = rq.read().decode('utf8')

    company_years = extract_years(std_html + bokslut_html)

    for company_year in company_years.values():
        company_year['org_nummer'] = org_nummer
        company_year['namn'] = namn
        insert('company_year', list(company_year.keys()), list(company_year.values()))

companies2fill_urls = query_all('SELECT url_allabolag, org_nummer, namn FROM company')
for company2fill in companies2fill_urls:
    fetch_company_data(*company2fill)
