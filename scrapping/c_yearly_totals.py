import sqlite3

from scrapping.db import conn, query_all, update


def fix_rows():
    conn.row_factory = sqlite3.Row
    company_years = query_all('SELECT * FROM company_year')
    for company_year in company_years:
        company_year_new = {}
        for k, v in dict(company_year).items():
            if v:
                company_year_new[k] = v.replace(' ', '').replace(',', '.').strip()
            else:
                company_year_new[k] = v
        update('company_year',
               'org_nummer = "%s" AND year = "%s" AND month = "%s"' % (company_year['org_nummer'], company_year['year'],
                                                                       company_year['month']),
               company_year_new)

conn.row_factory = sqlite3.Row
company_years = query_all('SELECT * FROM company_year')
totals = {}
for company_year in company_years:
    row_year = company_year['year']
    if totals.get(row_year) is None:
        totals[row_year] = {k: '' for k, v in dict(company_year).items()}

    for k, v in dict(company_year).items():
        if v:
            had_percent = False
            if '%' in v or '%' in totals[row_year][k]:
                had_percent = True
                v = v.replace('%', '')
                totals[row_year][k] = totals[row_year][k].replace('%', '')

            try:
                totals[row_year][k] = str(float(totals[row_year][k] or 0) + float(v))
            except ValueError:
                pass

            if had_percent:
                totals[row_year][k] += '%'


years = sorted(totals.keys())
for y in years:
    print(y)
    print({k: v for k, v in totals[y].items() if '%' not in v})
